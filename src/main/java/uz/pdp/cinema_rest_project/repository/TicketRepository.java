package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinema_rest_project.model.Ticket;
import uz.pdp.cinema_rest_project.model.enam.TicketStatus;
import uz.pdp.cinema_rest_project.projection.TicketProjection;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {


    @Query(nativeQuery = true, value = "select t.id as id,\n" +
            "       t.price as price,\n" +
            "       m.title as movieTitle\n" +
            "from ticket t\n" +
            "         join movie_session ms on t.movie_session_id = ms.id\n" +
            "join movie_announcements ma on ms.movie_announcement_id = ma.id\n" +
            "join movie m on ma.movie_id = m.id\n" +
            "where t.user_id = :userId and t.ticket_status = 'NEW'")
    List<TicketProjection> getTicketByUserId(Integer userId);

    List<Ticket> findAllByUserIdAndTicketStatus(Integer userId, TicketStatus ticketStatus);
}
