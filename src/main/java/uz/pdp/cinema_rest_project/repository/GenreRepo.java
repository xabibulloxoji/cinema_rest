package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinema_rest_project.model.Genre;
import uz.pdp.cinema_rest_project.projection.GenreProjection;

import java.util.List;
import java.util.UUID;

public interface GenreRepo extends JpaRepository<Genre, Integer> {
    @Query(value = "select cast(g.id as varchar), g.name as name from genres g" +
            " join movie_genre mg on g.id = mg.genre_id " +
            "join movie m on mg.movie_id = m.id " +
            "where m.id = :movieId", nativeQuery = true)
    List<GenreProjection> getGenresByMovieId(Integer movieId);
}
