package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema_rest_project.model.User;

public interface UserRepo extends JpaRepository<User, Integer> {
}
