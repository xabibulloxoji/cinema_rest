package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinema_rest_project.model.Hall;
import uz.pdp.cinema_rest_project.projection.HallAndTimesProjectionForSession;

import java.util.List;
import java.util.UUID;

public interface HallRepo extends JpaRepository<Hall, Integer> {
    @Query(nativeQuery = true, value = "select\n" +
            "       cast(h.id as varchar) as id,\n" +
            "       cast(h.name as varchar) as name,\n" +
            "       cast(ms.movie_announcement_id as varchar) as movieAnnouncementId,\n" +
            "       cast(ms.start_date_id as varchar) as startDateId\n" +
            "from movie_session ms\n" +
            "join hall h on h.id = ms.hall_id\n" +
            "where  ms.movie_announcement_id = :movieAnnouncementId\n" +
            "and ms.start_date_id = :startDateId")
    List<HallAndTimesProjectionForSession> getHallsAndTimesByMovieAnnouncementIdAndStartDateId(
            Integer movieAnnouncementId,
            Integer startDateId);


}
