package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema_rest_project.model.Actor;
import uz.pdp.cinema_rest_project.model.AttachmentContent;

public interface ActorRepo extends JpaRepository<Actor, Integer> {
}
