package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema_rest_project.model.PayType;

public interface PayTypeRepo extends JpaRepository<PayType, Integer> {
}
