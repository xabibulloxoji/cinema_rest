package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.cinema_rest_project.model.MovieAnnouncement;
import uz.pdp.cinema_rest_project.projection.CustomMovieAnnouncement;

import java.util.List;


public interface MovieAnnouncementRepository extends JpaRepository<MovieAnnouncement, Integer> {

    @Query(value = "select m.title,\n" +
            "       m.cover_image_id as coverImgId,\n" +
            "       m.description,\n" +
            "       m.duration_in_minutes as durationInMinutes,\n" +
            "       m.trailer_video_url as trailerVideoUrl,\n" +
            "       m.release_date as releaseDate,\n" +
            "       ma.is_active as isActive\n" +
            "from movie_announcements ma\n" +
            "         join movie m on ma.movie_id = m.id;", nativeQuery = true)
    List<CustomMovieAnnouncement> getAllMovieAnnouncement();

}
