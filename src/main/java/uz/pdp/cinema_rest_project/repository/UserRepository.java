package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema_rest_project.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

//    Optional<User> findByUsername(String username);
}
