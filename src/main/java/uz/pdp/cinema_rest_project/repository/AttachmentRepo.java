package uz.pdp.cinema_rest_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema_rest_project.model.Attachment;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;

public interface AttachmentRepo extends JpaRepository<Attachment, Integer> {
}
