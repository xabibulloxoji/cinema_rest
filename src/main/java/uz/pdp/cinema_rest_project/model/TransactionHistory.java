package uz.pdp.cinema_rest_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema_rest_project.model.template.AbsEntity;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "transaction_histories")
public class TransactionHistory {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//
//    @OneToOne
//    private User user;
//
//    @ManyToMany
////    @JoinColumn(name = "transaction_histories_tickets")
//    private List<Ticket> ticket;
//
//    private Date date;
//
//    @ManyToMany
//    private List<PayType> payType;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;
        @ManyToMany
        @JoinTable(name = "transaction_histories_tickets",
                joinColumns =@JoinColumn(name = "transaction_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "ticket_id", referencedColumnName = "id"))
        private List<Ticket> ticketList;

        private double amount;
        private boolean isRefunded;
        private LocalDate date=LocalDate.now();

        @OneToOne
        private PayType payType;

        private String paymentIntent;

        public TransactionHistory(List<Ticket> ticketList, double amount, boolean isRefunded,
                                  PayType payType, String paymentIntent) {
            this.ticketList = ticketList;
            this.amount = amount;
            this.isRefunded = isRefunded;
            this.payType = payType;
            this.paymentIntent = paymentIntent;
        }
}
