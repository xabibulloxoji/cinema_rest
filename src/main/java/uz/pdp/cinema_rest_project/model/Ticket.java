package uz.pdp.cinema_rest_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema_rest_project.model.enam.*;
import uz.pdp.cinema_rest_project.model.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private MovieSession movieSession;

    @OneToOne
    private Seat seat;

    private String qrCode;

    private double price;

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus = TicketStatus.NEW;

    public Ticket(MovieSession movieSession, Seat seat, Double price, User user) {
        this.movieSession = movieSession;
        this.seat = seat;
        this.price = price;
        this.user = user;
    }

}
