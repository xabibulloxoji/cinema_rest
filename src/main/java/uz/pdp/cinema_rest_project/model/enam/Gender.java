package uz.pdp.cinema_rest_project.model.enam;

public enum Gender {
    MALE,
    FEMALE
}
