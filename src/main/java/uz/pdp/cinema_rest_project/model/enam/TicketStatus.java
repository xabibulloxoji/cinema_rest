package uz.pdp.cinema_rest_project.model.enam;

public enum TicketStatus {
    NEW,
    PURCHASED,
    REFUNDED
}
