package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.model.Permission;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;


public interface PermissionService {

    public ApiResponse getAllPermission();

    public ApiResponse getPermissionById(Integer id);

    public ApiResponse addPermission(Permission permission);

    public ApiResponse updatePermission(Integer id, Permission permission);

    public ApiResponse deletePermission(Integer id);

}
