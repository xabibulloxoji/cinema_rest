package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.cinema_rest_project.dto.MovieDto;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;

import java.io.IOException;

@Service
public interface MovieService {

    public ApiResponse getAllMovies();

    public ApiResponse getMovieById(Integer id);

    public ApiResponse addMovie(MovieDto movieDto,Integer id ,
                                MultipartFile request) throws IOException;

    public ApiResponse updateMovie(Integer id, MovieDto movieDto);

    public ApiResponse deleteMovie(Integer id);

    HttpEntity getAllMovies(
            int page,
            int size,
            String search,
            String sort,
            boolean direction
    );


}
