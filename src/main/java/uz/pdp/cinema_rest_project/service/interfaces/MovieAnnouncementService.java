package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.dto.MovieAnnouncementDto;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;

@Service
public interface MovieAnnouncementService {

    ApiResponse getAllMovieAnnouncement();

    ApiResponse getMovieAnnouncementById(Integer id);

    ApiResponse addAndUpdateMovieAnnouncement(MovieAnnouncementDto movieAnnouncementDto,
                                                Integer id);

    ApiResponse deleteMovieAnnouncement(Integer id);
}
