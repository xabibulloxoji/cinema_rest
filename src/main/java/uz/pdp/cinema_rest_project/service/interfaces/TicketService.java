package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.http.HttpEntity;
import uz.pdp.cinema_rest_project.dto.TicketDto;
import uz.pdp.cinema_rest_project.model.Ticket;


public interface TicketService {


    HttpEntity createAndAddTicket(TicketDto ticketDto);


    void scheduleDeleteTicket(Ticket ticket);

    HttpEntity purchaseTicket(Integer ticketId);

    HttpEntity refundTicket(Integer ticketId);

    HttpEntity getTicketsByUserId(Integer userId);
}
