package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.http.HttpEntity;
import uz.pdp.cinema_rest_project.dto.MovieSessionDto;

public interface MovieSessionService {

    HttpEntity getAllMovieSessions(
            int page,
            int size,
            String search
    );

    HttpEntity<?> addMovieSession(MovieSessionDto movieSessionDto);


}
