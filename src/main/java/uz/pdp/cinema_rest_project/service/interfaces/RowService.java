package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.dto.RowDto;
import uz.pdp.cinema_rest_project.model.Row;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;

@Service
public interface RowService {

    public ApiResponse getAllRows();

    public ApiResponse getRowById(Integer id);

    public  ApiResponse addAndAddRow(RowDto rowDto, Integer id);

    public ApiResponse deleteRow(Integer id);

}
