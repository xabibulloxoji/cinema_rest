package uz.pdp.cinema_rest_project.service.interfaces;

import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.model.Hall;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;

@Service
public interface HallService {

    public ApiResponse getAllHalls();

    public ApiResponse getHallById(Integer id);

    public ApiResponse addHall(Hall hall, Integer id);

//    public ApiResponse updateHall(Integer id, Hall hall);

    public ApiResponse deleteHall(Integer id);

}
