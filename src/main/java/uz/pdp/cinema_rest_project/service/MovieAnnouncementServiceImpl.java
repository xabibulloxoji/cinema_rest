package uz.pdp.cinema_rest_project.service;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.dto.MovieAnnouncementDto;
import uz.pdp.cinema_rest_project.model.Movie;
import uz.pdp.cinema_rest_project.model.MovieAnnouncement;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;
import uz.pdp.cinema_rest_project.projection.CustomMovieAnnouncement;
import uz.pdp.cinema_rest_project.projection.CustomMovieById;
import uz.pdp.cinema_rest_project.repository.MovieAnnouncementRepository;
import uz.pdp.cinema_rest_project.repository.MovieRepo;
import uz.pdp.cinema_rest_project.service.interfaces.MovieAnnouncementService;

import javax.swing.*;
import java.util.List;
import java.util.Optional;

@Service
public class MovieAnnouncementServiceImpl implements MovieAnnouncementService {
    @Autowired
    MovieAnnouncementRepository movieAnnouncementRepository;
    @Autowired
    MovieRepo movieRepo;

    @Override
    public ApiResponse getAllMovieAnnouncement() {
        List<CustomMovieAnnouncement> movieAnnouncements = movieAnnouncementRepository.getAllMovieAnnouncement();
        if (movieAnnouncements.size() != 0) {
            return new ApiResponse("Success", true, movieAnnouncements);
        }
        return new ApiResponse("MovieAnnouncement is not found", false, null);
    }

    @Override
    public ApiResponse getMovieAnnouncementById(Integer id) {
        Optional<CustomMovieById> movieById = movieRepo.getMovieById(id);
        if (!movieById.isPresent()) {
            return new ApiResponse("MovieAnnouncement is not found", false,
                    null);
        }
        CustomMovieById customMovieById = movieById.get();
        return new ApiResponse("Success", true, customMovieById);
    }

    @Override
    public ApiResponse addAndUpdateMovieAnnouncement(MovieAnnouncementDto movieAnnouncementDto,
                                                       Integer id) {
        if (id != null) {
            Optional<MovieAnnouncement> optionalMovieAnnouncement = movieAnnouncementRepository.findById(id);
            if (optionalMovieAnnouncement.isPresent()) {
                MovieAnnouncement movieAnnouncement = optionalMovieAnnouncement.get();
                Optional<Movie> optionalMovie =
                        movieRepo.findById(movieAnnouncement.getMovie().getId());
                if (!optionalMovie.isPresent()) {
                    return new ApiResponse("Movie is not found", false, null);
                }
                Movie movie = optionalMovie.get();

                movieAnnouncement.setIsActive(movieAnnouncementDto.getIsActive());

                Optional<Movie> optionalMovie1 = movieRepo.findById(movieAnnouncementDto.getMovieId());
                if (!optionalMovie1.isPresent()) {
                    return new ApiResponse("Movie is not found", false, null);
                }
                Movie movie1 = optionalMovie1.get();
                movieAnnouncement.setMovie(movie1);
                MovieAnnouncement save = movieAnnouncementRepository.save(movieAnnouncement);
                return new ApiResponse("Sucess", true, save);
            }
        }

        MovieAnnouncement movieAnnouncement = new MovieAnnouncement();
        Optional<Movie> optionalMovie = movieRepo.findById(movieAnnouncementDto.getMovieId());
        if (!optionalMovie.isPresent()) {
            return new ApiResponse("Movie is not found", false);
        }
        Movie movie = optionalMovie.get();

        movieAnnouncement.setIsActive(movieAnnouncementDto.getIsActive());
        movieAnnouncement.setMovie(movie);
        movieAnnouncementRepository.save(movieAnnouncement);

        return new ApiResponse("Success", true);
    }

    @Override
    public ApiResponse deleteMovieAnnouncement(Integer id) {
        Optional<MovieAnnouncement> optionalMovieAnnouncement = movieAnnouncementRepository.findById(id);
        if (optionalMovieAnnouncement.isPresent()) {
            MovieAnnouncement movieAnnouncement = optionalMovieAnnouncement.get();
            movieAnnouncementRepository.delete(movieAnnouncement);
            return new ApiResponse("Success", true);
        }
        return new ApiResponse("MovieAnnaouncemenmt is not found", false);

    }
}
