package uz.pdp.cinema_rest_project.service;

import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.cinema_rest_project.dto.TicketDto;
import uz.pdp.cinema_rest_project.model.*;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;
import uz.pdp.cinema_rest_project.repository.*;
import uz.pdp.cinema_rest_project.service.interfaces.TicketService;

import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    MovieRepo movieRepo;
    @Autowired
    MovieSessionRepository movieSessionRepository;
    @Autowired
    SeatRepo seatRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    TicketRepository ticketRepository;

    @Override
    public HttpEntity createAndAddTicket(TicketDto ticketDto) {

        Ticket ticket = new Ticket();

        Optional<MovieSession> optionalMovieSession = movieSessionRepository.findById(ticketDto.getMovieSessionId());
        if (!optionalMovieSession.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("MovieSession is not found", false));
        }
        MovieSession movieSession = optionalMovieSession.get();

        Optional<Seat> optionalSeat = seatRepo.findById(ticketDto.getSeatId());
        if (!optionalSeat.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Seat is not found", false));
        }

        Seat seat = optionalSeat.get();

        Optional<User> optionalUser = userRepo.findById(1);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("User is not found", false));
        }

        User user = optionalUser.get();
        double minPrice = movieSession.getMovieAnnouncement().getMovie().getMinPrice();
        ticket.setSeat(seat);
        ticket.setUser(user);
        ticket.setPrice(minPrice);
        ticket.setMovieSession(movieSession);
        ticketRepository.save(ticket);
        return ResponseEntity.ok(new ApiResponse("Successfully added", true));
    }

    @Override
    public void scheduleDeleteTicket(Ticket ticket) {

    }

    @Override
    public HttpEntity purchaseTicket(Integer ticketId) {
        return null;
    }

    @Override
    public HttpEntity refundTicket(Integer ticketId) {
        return null;
    }

    @Override
    public HttpEntity getTicketsByUserId(Integer userId) {
        return null;
    }
}
