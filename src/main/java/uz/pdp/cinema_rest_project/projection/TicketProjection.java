package uz.pdp.cinema_rest_project.projection;

import java.time.LocalDate;
import java.util.UUID;

public interface TicketProjection {

    Integer  getId();

    Double getPrice();

    String getMovieTitle();

    // TODO: 3/28/2022  ...
}
