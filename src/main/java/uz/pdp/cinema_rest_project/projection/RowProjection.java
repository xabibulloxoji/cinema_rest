package uz.pdp.cinema_rest_project.projection;

import java.util.UUID;

//@Projection(types = Row.class)
public interface RowProjection {

    Integer getId();

    Integer getNumber();

    String getHallName();

}
