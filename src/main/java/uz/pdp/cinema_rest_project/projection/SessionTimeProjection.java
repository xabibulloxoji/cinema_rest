package uz.pdp.cinema_rest_project.projection;

import java.time.LocalTime;

public interface SessionTimeProjection {

    Integer getId();

    LocalTime getTime();

    Integer getSessionId();

}
