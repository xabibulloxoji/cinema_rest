package uz.pdp.cinema_rest_project.projection;

import org.springframework.beans.factory.annotation.Value;
import uz.pdp.cinema_rest_project.model.Movie;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

//@Projection(types = Movie.class)
public interface CustomMovieById {

    Integer getId();

    String getTitle();

    Integer getCoverImgId();

    LocalDate getReleaseDate();


    // todo get more fields

    @Value("#{@genreRepo.getGenresByMovieId(target.id)}")
    List<GenreProjection> getGenres();


}
