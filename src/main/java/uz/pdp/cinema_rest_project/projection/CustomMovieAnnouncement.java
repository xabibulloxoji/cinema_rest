package uz.pdp.cinema_rest_project.projection;

import org.apache.tomcat.jni.Local;
import org.springframework.data.rest.core.config.Projection;
import sun.font.CStrike;
import uz.pdp.cinema_rest_project.model.MovieAnnouncement;

import java.time.LocalDate;

@Projection(types = MovieAnnouncement.class)
public interface CustomMovieAnnouncement {

    String getTitle();

    Integer getCoverImgId();

    String getDescription();

    Integer getDurationInMinutes();

    String getTrailerVideoUrl();

    LocalDate getReleaseDate();

    Boolean getIsActive();

}
