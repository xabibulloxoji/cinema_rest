package uz.pdp.cinema_rest_project.projection;

import java.util.UUID;

public interface GenreProjection {


    Integer getId();

    String getName();
}
