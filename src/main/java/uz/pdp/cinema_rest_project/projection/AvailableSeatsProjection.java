package uz.pdp.cinema_rest_project.projection;

import java.util.UUID;

public interface AvailableSeatsProjection {

    Integer getId();

    Integer getSeatNumber();

    Integer getRowNumber();

    Boolean getAvailable();
}
