package uz.pdp.cinema_rest_project.projection;

import java.sql.Date;
import java.util.Iterator;
import java.util.UUID;

public interface CustomMovie {
    Integer getId();

    String getTitle();

    Date getReleaseDate();

   Integer getCoverImgId();
}
