package uz.pdp.cinema_rest_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaRestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaRestProjectApplication.class, args);
    }

}
