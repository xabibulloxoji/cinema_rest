package uz.pdp.cinema_rest_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class TicketDto {
    private Integer movieSessionId;
    private Integer seatId;
}
