package uz.pdp.cinema_rest_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieSessionDto {

    private Integer movieAnnouncementId;

    private List<ReservedHallDto> reservedHallDtoList;

}
