package uz.pdp.cinema_rest_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class ActorDto {
    private String fullName;
    private int photoId;
}
