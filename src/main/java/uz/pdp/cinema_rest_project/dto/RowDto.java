package uz.pdp.cinema_rest_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RowDto {
    private String name;
    private Integer hallId;
}
