package uz.pdp.cinema_rest_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieAnnouncementDto {


    private Integer movieId;

    private Boolean isActive;

}
