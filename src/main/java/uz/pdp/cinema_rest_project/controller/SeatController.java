package uz.pdp.cinema_rest_project.controller;

import com.sun.deploy.net.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinema_rest_project.service.SeatService;

import java.util.UUID;

@RestController
@RequestMapping("/api/seat")
public class SeatController {

//    public HttpResponse getAllSeat
    @Autowired
    SeatService seatService;

    @GetMapping("/available-seats/{movieSessionId}")
    public HttpEntity<?> getAvailableSeatsBySessionId(@PathVariable Integer movieSessionId) {
        return seatService.getAvailableSeatsBySessionId(movieSessionId);
    }
}
