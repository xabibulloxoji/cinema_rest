package uz.pdp.cinema_rest_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema_rest_project.dto.MovieAnnouncementDto;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;
import uz.pdp.cinema_rest_project.projection.CustomMovieById;
import uz.pdp.cinema_rest_project.repository.MovieRepo;
import uz.pdp.cinema_rest_project.service.MovieAnnouncementServiceImpl;

import java.util.Optional;

@RestController
@RequestMapping("/api/movieAnnouncement")
public class MovieAnnouncementController {

    @Autowired
    MovieAnnouncementServiceImpl movieAnnouncementService;
    @Autowired
    MovieRepo movieRepo;

    @GetMapping
    public HttpEntity<?> getAllMovieAnnouncement(){
        ApiResponse allMovieAnnouncement = movieAnnouncementService.getAllMovieAnnouncement();
        return ResponseEntity.status(allMovieAnnouncement.isSuccess()?200: 404).body(allMovieAnnouncement);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getMovieAnnouncementById(@PathVariable Integer id){
        ApiResponse movieAnnouncementById = movieAnnouncementService.getMovieAnnouncementById(id);
        return ResponseEntity.status(movieAnnouncementById.isSuccess()?200: 404).body(movieAnnouncementById);
    }

    @PostMapping
    public HttpEntity<?> addMovieAnnouncement(@RequestBody MovieAnnouncementDto movieAnnouncementDto
            , Integer id){
        ApiResponse apiResponse = movieAnnouncementService.addAndUpdateMovieAnnouncement(movieAnnouncementDto, id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:404).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> updateMovieAnnouncement(@PathVariable Integer id, @RequestPart(name =
            "josn") MovieAnnouncementDto movieAnnouncementDto){
        ApiResponse response = movieAnnouncementService.addAndUpdateMovieAnnouncement(movieAnnouncementDto, id);
        return ResponseEntity.status(response.isSuccess()?200:400).body(response);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteMovieAnnouncement(@PathVariable Integer id){
        ApiResponse response = movieAnnouncementService.deleteMovieAnnouncement(id);
        return ResponseEntity.status(response.isSuccess()?200:404).body(response);
    }

}
