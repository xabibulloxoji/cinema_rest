package uz.pdp.cinema_rest_project.controller;

import com.stripe.Stripe;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.Event;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema_rest_project.model.Ticket;
import uz.pdp.cinema_rest_project.model.TransactionHistory;
import uz.pdp.cinema_rest_project.model.enam.TicketStatus;
import uz.pdp.cinema_rest_project.repository.PayTypeRepo;
import uz.pdp.cinema_rest_project.repository.TicketRepository;
import uz.pdp.cinema_rest_project.repository.TransactionHistoryRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class StripePaymentController {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    PayTypeRepo payTypeRepo;

    @Autowired
    TransactionHistoryRepository transactionHistoryRepository;

    @Value("${STRIPE_API_KEY}")
    String stripeApiKey;

    @RequestMapping("/success")
    public ResponseEntity<?> getSuccessMsg() {
        return ResponseEntity.ok("To'lov amalga oshirildi");
    }

    @RequestMapping("/payment-failed")
    public ResponseEntity<?> getFailed(){
        return ResponseEntity.ok("To'lov amalga oshirilmadi");
    }

    @RequestMapping(value = "stripe", method = RequestMethod.POST)
    public Object handle(@RequestBody String payload,
                         @RequestHeader("Stripe-Signature") String sigHeader){
        Stripe.apiKey = stripeApiKey;

        String endPoint = "whsec_deb48180db7db57e00d753dc2398d77ff0eca792adae9323fc20ec72f3ec9efd";

        Event event = null;

        try {
            event = Webhook.constructEvent(payload, sigHeader, endPoint);

        } catch (SignatureVerificationException e) {
            e.printStackTrace();
        }

        if (event.getType().equals("checkout.session.completed")) {
            Session sessionObject = (Session) event.getDataObjectDeserializer().getObject().get();

            fulfillOrder(sessionObject);
        }
        return "";
    }

    private void fulfillOrder(Session sessionObject) {

        List<Ticket> ticketList = ticketRepository.findAllByUserIdAndTicketStatus(Integer.parseInt(sessionObject.getClientReferenceId()),
                TicketStatus.NEW);
        for (Ticket ticket : ticketList) {
            ticket.setTicketStatus(TicketStatus.PURCHASED);
            ticketRepository.save(ticket);
        }

        transactionHistoryRepository.save(new TransactionHistory(ticketList,
                sessionObject.getAmountTotal(), false, payTypeRepo.findById(1).get(),
                sessionObject.getPaymentIntent()));



    }
}
