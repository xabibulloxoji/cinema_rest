package uz.pdp.cinema_rest_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.cinema_rest_project.model.Director;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;
import uz.pdp.cinema_rest_project.service.ActorService;
import uz.pdp.cinema_rest_project.service.DirectorService;

import java.io.IOException;

@RestController
@RequestMapping("/api/director")
public class DirectorController {

    @Autowired
    DirectorService directorService;
    @GetMapping
    public ApiResponse getAllDirector(){
        return directorService.getAllDirectors();
    }

    @GetMapping("/{id}")
    public ApiResponse getDirectorById(@PathVariable Integer id){
        return directorService.getDirectorById(id);
    }

    @PostMapping
    public ApiResponse addDirector(@RequestPart(name = "director") Director director,
                                   @RequestPart(name = "file") MultipartFile request) throws IOException {
        return directorService.addDirector(director, request);
    }

    @PutMapping("/{id}")
    public ApiResponse updateDirector(@PathVariable Integer id,
                                      @RequestPart(name = "director") Director director,
                                      MultipartFile file) throws IOException {
        return directorService.updateDirector(id, director, file);
    }

    @DeleteMapping("/{id}")
    public ApiResponse deleteDirector(@PathVariable Integer id){
        return directorService.deleteDirector(id);
    }
}
