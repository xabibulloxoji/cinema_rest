package uz.pdp.cinema_rest_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.cinema_rest_project.model.Actor;
import uz.pdp.cinema_rest_project.payLoad.ApiResponse;
import uz.pdp.cinema_rest_project.repository.ActorRepo;
import uz.pdp.cinema_rest_project.repository.AttachmentContentRepo;
import uz.pdp.cinema_rest_project.repository.AttachmentRepo;
import uz.pdp.cinema_rest_project.service.ActorService;

import java.io.IOException;

@RestController
@RequestMapping("/api/actor")
public class ActorController {

    @Autowired
    ActorRepo actorRepo;

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    AttachmentContentRepo attachmentContentRepo;

    @Autowired
    ActorService actorService;

    @GetMapping
    public ApiResponse getActors() {
        return actorService.getAllActors();
    }

    @GetMapping("/{id}")
    public ApiResponse getActorById(@PathVariable Integer id) {
        return actorService.getActorById(id);
    }

    @PostMapping
    public ApiResponse addActor(@RequestPart(name = "actor") Actor actor,
                               @RequestPart(name = "file") MultipartFile request) throws IOException {
        return actorService.addActor(actor, request);
    }

    @PutMapping("/{id}")
    public ApiResponse updateActor(@PathVariable Integer id,
                                     @RequestPart(name = "actor") Actor actor,
                                     @RequestPart(name = "file") MultipartFile file) throws IOException {
        return actorService.updateActor(id, actor, file);
    }

    @DeleteMapping("/{id}")
    public ApiResponse deleteActor(@PathVariable Integer id){
         return actorService.deleteActor(id);
    }

}
