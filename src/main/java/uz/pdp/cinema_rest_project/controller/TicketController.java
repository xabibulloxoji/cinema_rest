package uz.pdp.cinema_rest_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema_rest_project.dto.TicketDto;
import uz.pdp.cinema_rest_project.service.TicketServiceImpl;
import uz.pdp.cinema_rest_project.service.interfaces.TicketService;

import java.util.UUID;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {

    @Autowired
    TicketServiceImpl ticketService;

//    @GetMapping("/get-my-tickets")
//    public HttpEntity getTicketsByUserId() {
//        // TODO: 3/28/2022 GET CURRENT USER'S ID
//        Integer userId = null;
//        return ticketService.getTicketsByUserId(userId);
//    }
//
//
    @PostMapping("/add-to-cart")
    public HttpEntity createAndAddTicket(@RequestBody TicketDto ticketDto) {
        return ticketService.createAndAddTicket(ticketDto);
    }
//
//
//    @GetMapping("/purchase/{ticketId}")
//    public HttpEntity purchaseTicket(@PathVariable Integer ticketId) {
//        return ticketService.purchaseTicket(ticketId);
//    }

}
