package uz.pdp.cinema_rest_project.controller;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.cinema_rest_project.projection.TicketProjection;
import uz.pdp.cinema_rest_project.repository.TicketRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    @Autowired
    TicketRepository ticketRepository;

    @Value("${STRIPE_API_KEY}")
    String stripeApiKey;

    @GetMapping("/purchase")
    public HttpEntity<?> purchaseTicket() throws StripeException {
        Integer userId = 1;
        List<TicketProjection> ticketByUserId = ticketRepository.getTicketByUserId(userId);
        List<SessionCreateParams.LineItem> lineItems = new ArrayList<>();
        Stripe.apiKey = stripeApiKey;


        for (TicketProjection ticketProjection : ticketByUserId) {

            SessionCreateParams.LineItem.PriceData.ProductData productData = SessionCreateParams.LineItem.PriceData.ProductData
                    .builder()
                    .setName(ticketProjection.getMovieTitle())
                    .build();


            SessionCreateParams.LineItem.PriceData priceData = SessionCreateParams.LineItem.PriceData
                    .builder()
                    .setProductData(productData)
                    .setCurrency("usd")
                    .setUnitAmount(ticketProjection.getPrice().longValue())
                    .build();


//        Stripedagi ticket malumotlarini
            SessionCreateParams.LineItem lineItem = SessionCreateParams.LineItem
                    .builder()
                    .setPriceData(priceData)
                    .setQuantity(1L)
                    .build();

            lineItems.add(lineItem);
        }

            // current data
            SessionCreateParams params = SessionCreateParams
                    .builder()
                    .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                    .setMode(SessionCreateParams.Mode.PAYMENT)
                    .addAllLineItem(lineItems)
                    .setClientReferenceId("1")
                    .setCancelUrl("http://localhost:8080/payment-failed")
                    .setSuccessUrl("http://localhost:8080/success")
                    .build();


        Session session = Session.create(params);

        String url = session.getUrl();
        return ResponseEntity.ok(url);
    }

}
